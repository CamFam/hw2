from unittest import TestCase
from app import app
import json

class TestFlask(TestCase):

	def setUp(self):
		self.client = app.test_client()

	def test_coe332(self):
		# Test the / root route
		result = self.client.get('/')

		# The http status code should be OK
		self.assertEqual(result.status, "200 OK")

		# Decode the binary result returned by the test client into a utf-8 string
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)

		# Assert that the returned structure has the data we want
		self.assertIn("instructors", data_dictionary)
		self.assertIn("meeting", data_dictionary)
		self.assertIn("assignments", data_dictionary)

	### Instructors route tests	
	def test_instructors(self):
		# Test the /instructors route
		result = self.client.get('/instructors')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)
		self.assertIn("name", data_dictionary)
		self.assertIn("email", data_dictionary)

	def test_instructor_ari(self):
		# Test the /instructors/1 route
		result = self.client.get('/instructors/1')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)
		self.assertIn("Ari Kahn", data_dictionary)

	def test_instructor_nobody(self):
		# Test the /instructors/4 route
		result = self.client.get('/instructors/4')
		self.assertEqual(result.status, "500 INTERNAL SERVER ERROR")

	def test_instructor_name(self):
		# Test the /instructors/1/name route
		result = self.client.get('/instructors/1/name')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '"Ari Kahn"\n'
		self.assertEqual(data_string, expected_string)

	def test_instructor_email(self):
		# Test the /instructors/1/email route
		result = self.client.get('/instructors/1/email')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '"akahn@tacc.utexas.edu"\n'
		self.assertEqual(data_string, expected_string)

	### Assignment tests
	def test_assignments(self):
		# Test the /assignments route
		result = self.client.get('/assignments')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)
		#expected_string = '[{"name":"hw1","url":"https://bitbucket.org/jchuahtacc/coe332-f2019-hw1","points":10},{"name":"hw2","url":"https://bitbucket.org/jchuahtacc/coe332-f2019-hw2","points":20}]\n'
		self.assertIn("name", data_dictionary)
		self.assertIn("url", data_dictionary)
		self.assertIn("points", data_dictionary)
	
	def test_assignments_one(self):
		# Test the /assignments/1 route
		result = self.client.get('/assignments/1')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)
		#expected_string = '{"name":"hw1","url":"https://bitbucket.org/jchuahtacc/coe332-f2019-hw1","points":10}\n'
		self.assertIn("hw1", data_dictionary)

	def test_assignments_none(self):
		# Test the /assignments/3 route
		result = self.client.get('/assignments/3')
		self.assertEqual(result.status, "500 INTERNAL SERVER ERROR")

	def test_assignments_name(self):
		# Test the /assignments/name route
		result = self.client.get('/assignments/1/name')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '"hw1"\n'
		self.assertEqual(data_string, expected_string)

	def test_assignments_url(self):
		# Test the /assignments/1/url route
		result = self.client.get('/assignments/1/url')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '"https://bitbucket.org/jchuahtacc/coe332-f2019-hw1"\n'
		self.assertEqual(data_string, expected_string)

	def test_assignments_points(self):
		# Test the /assignments/1/points route
		result = self.client.get('/assignments/1/points')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = "10\n"	
		self.assertEqual(data_string, expected_string)

	def test_post_assignment(self):
		# Test that we can post a new assignment to /assignments
		# Testing json dictionary equivalence is weird, so we will make our best effort
		# Create a dictionary
		new_assignment = { 'name' : 'hw3', 'url' : 'https://bitbucket.org/jchuahtacc/coe332-f2019-hw3', 'points': 10 }

		# Post the string representation of the dictionary
		result = self.client.post('/assignments', data=json.dumps(new_assignment), content_type="application/json")
		self.assertEqual(result.status, "200 OK")
		result = self.client.get('/assignments')
		self.assertEqual(result.status, "200 OK")
		assignments = json.loads(result.data.decode("utf-8"))
		self.assertEqual(json.dumps(assignments[2], sort_keys=True), json.dumps(new_assignment, sort_keys=True))

	### Meeting route tests
	def test_meeting(self):
		# Test the /meeting route
		result = self.client.get('/meeting')
		self.assertEqual(result.status, "200 OK")	
		data_string = result.data.decode("utf-8")
		data_dictionary = json.dumps(data_string)
		#test time	
		self.assertIn("days", data_dictionary)
		self.assertIn("start", data_dictionary)
		self.assertIn("end", data_dictionary)
		self.assertIn("location", data_dictionary)

	def test_meeting_days(self):
		# Test the /meeting/days route
		result = self.client.get('/meeting/days')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '["Tuesday","Thursday"]\n'
		self.assertEqual(data_string, expected_string)


	def test_meeting_start(self):
		# Test the /meeting/start route
		result = self.client.get('/meeting/start')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '1100\n'
		self.assertEqual(data_string, expected_string)

	def test_meeting_end(self):
		# Test the /meeting/start route
		result = self.client.get('/meeting/end')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '1330\n'
		self.assertEqual(data_string, expected_string)

	def test_meeting_location(self):
		# Test the /meeting/location route
		result = self.client.get('/meeting/location')
		self.assertEqual(result.status, "200 OK")
		data_string = result.data.decode("utf-8")
		expected_string = '"GDC 3.248"\n'								
		self.assertEqual(data_string, expected_string)
